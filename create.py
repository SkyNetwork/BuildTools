import sys, os, shutil, errno, subprocess, argparse, urllib.request, zipfile, colorama
from colorama import Fore, Style

projectBase = ""

def getFile(url, path):
	with urllib.request.urlopen(url) as response, open(path, 'wb') as path:
		shutil.copyfileobj(response, path)
		
def mkDir(name):
	try:
		os.makedirs(name)
	except OSError as e:
		if e.errno != errno.EEXIST:
			raise
			
def remSilent(file):
    try:
        os.remove(file)
    except OSError as e:
        if e.errno != errno.ENOENT:
            raise
			
def modFile(replacements, file):
	lines = []
	with open(file) as infile:
		for line in infile:
			for src, target in replacements.items():
				line = line.replace(src, target)
			lines.append(line)
		
	with open(file, 'w') as outfile:
		for line in lines:
			outfile.write(line)
		
def git(*args):
	return subprocess.check_call(['git'] + list(args), cwd=projectBase)

def main(argv):
	parser = argparse.ArgumentParser(prog="DevKit")
	parser.add_argument("-n", "--name", help="Project Name", required=True)
	parser.add_argument("-p", "--path", help="Project Path", required=True)
	parser.add_argument("-d", "--description", help="Project Description", required=True)
	parser.add_argument("-c", "--creator", help="Developer Name", required=True)
	parser.add_argument("-w", "--web", help="Developer Website", required=True)
	
	args = parser.parse_args()
	
	colorama.init()
	
	print(Fore.GREEN + "Initializing Project .." + Style.RESET_ALL)
	global projectBase
	projectBase = os.path.join(args.path, args.name)
	
	mkDir(projectBase)
	
	print(Fore.GREEN + "Generating Replacements .." + Style.RESET_ALL)
	replacements = {
		"{DEV_NAME}":args.creator, 
		"{DEV_WEBSITE}":args.web, 
		"{PROJ_NAME}":args.name,
		"{PROJ_VERSION}":"0.0.1",
		"{PROJ_DESCRIPTION}":args.description
	}
	
	print(Fore.GREEN + "Downloading PluginTemplate.zip .." + Style.RESET_ALL)
	getFile("https://www.lawlsec.org/skynet/dev/PluginTemplate.zip", os.path.join(projectBase, "PluginTemplate.zip"))
	
	print(Fore.GREEN + "Extracting PluginTemplate.zip .." + Style.RESET_ALL)
	zip_ref = zipfile.ZipFile(os.path.join(projectBase, "PluginTemplate.zip"), 'r')
	zip_ref.extractall(projectBase)
	zip_ref.close()
	
	print(Fore.GREEN + "Fixing Directory Structure.." + Style.RESET_ALL)
	mkDir(os.path.join(projectBase, "src", "main", "java", "com", "skynetwork", args.creator, args.name))
	
	shutil.move(
		os.path.join(projectBase, "src", "main", "java", "TemplateMain.java"),
		os.path.join(projectBase, "src", "main", "java", "com", "skynetwork", args.creator, args.name, args.name + ".java")
	)
	
	print(Fore.GREEN + "Replacing Variables in Templates .." + Style.RESET_ALL)
	
	print("  .. pom.xml")
	modFile(replacements, os.path.join(projectBase, "pom.xml"))
	
	print("  .. Readme.md")
	modFile(replacements, os.path.join(projectBase, "Readme.md"))
	
	print("  .. plugin.yml")
	modFile(replacements, os.path.join(projectBase, "src", "main", "resources", "plugin.yml"))
	
	print("  .. " + args.name + ".java")
	modFile(replacements, os.path.join(projectBase, "src", "main", "java", "com", "skynetwork", args.creator, args.name, args.name + ".java"))
	
	print(Fore.GREEN + "Cleaning Temp Files .." + Style.RESET_ALL)
	remSilent(os.path.join(projectBase, "PluginTemplate.zip"))

	print(Fore.GREEN + "Running Git Init .." + Style.RESET_ALL)
	git("init")
	
	print("")
	print(Fore.CYAN)
	print("Next Steps (yes you have to do something too):")
	print("   1. Open IntelliJ")
	print("   2. Select \"Import Project\"")
	print("   3. Browse to and select " + projectBase)
	print("   4. Select \"Import project from external model\"")
	print("   5. Select the \"Maven\" model from the list")
	print("   6. Check the \"Import Maven Projects Automatically\"")
	print("   7. Check \"Automaticall Download\" \"Sources\" and \"Documentation\"")
	print("   8. Ensure that \"com.skynetwork." + args.creator + "." + args.name + ":" + args.name + ":0.0.1\" is checked")
	print("   9. Select JDK 1.8")
	print("  10. Click \"Finish\"")
	print("")
	print(Fore.GREEN + "All Set!" + Style.RESET_ALL)
	

if __name__ == "__main__":
   main(sys.argv[1:])