package com.skynetwork.{DEV_NAME}.{PROJ_NAME};

import org.bukkit.plugin.java.JavaPlugin;

public class {PROJ_NAME} extends JavaPlugin {
    public static {PROJ_NAME} Instance;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        Instance = this;
    }
	
	@Override
	public void onDisable() {
		// Clean up after yourself..
	}

    public static {PROJ_NAME} getInstance() {
        return Instance;
    }
}
