### SkyNetwork Dev Kit
This kit makes it easier to develop Spigot plugins for SkyNetwork.

You do not need to download the whole kit to use it, you just need the individual scripts you want to use, for example if you want to create a project with the tool just download the `create.py` file.

##### System Requirements
Git
Maven
Python 3.6

##### Python Requirements
Prompter (`pip install prompter`)
Colorama (`pip install colorama`)

##### Maven Settings Script (*mvnSet.py*)
The Maven Settings Script will download a standard Maven `settings.xml` to read from SkyNetwork's private Maven repo. This repo is where the SkyNetwork SDK is located. In order to use SkyNetwork SDK you need these settings in your `~/.m2/settings.xml`

Usage: `py mvnSet.py`

##### Create Script (*create.py*)
The Create Script creates a basic skeleton project for you so that you can easily import the project to IntelliJ Idea. This includes the proper structure and Maven `pom.xml` for use with Bukkit and the SkyNetwork SDK.

Usage: `py create.py {args}`

Args (ALL REQUIRED):
```
-n or --name        : Project Name
-p or --path        : Project Path
-d or --description : Project Description
-c or --creator     : Developer Name
-w or --web         : Developer Website
```

Example: `py create.py --name "Test" --path "C:\Users\Shellcode\Desktop" --description "Shellcode Test" --creator "Shellcode" --web "https://www.lawlsec.org"`

Example: `py create.py -n "Test" -p "C:\Users\Shellcode\Desktop" -d "Shellcode Test" -c "Shellcode" -w "https://www.lawlsec.org"`
