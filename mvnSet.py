import sys, os, shutil, colorama, urllib.request
from pathlib import Path
from prompter import yesno
from colorama import Fore, Style

def getFile(url, path):
	with urllib.request.urlopen(url) as response, open(path, 'wb') as path:
		shutil.copyfileobj(response, path)
		
def main(argv):
	colorama.init()

	print(Fore.YELLOW)
	maven = str(os.path.join(Path.home(), ".m2"))
	if os.path.isdir(maven):
		if yesno("Install modified \"settings.xml\"? (OVERWRITE IF EXISTS!)"):
			print(Fore.GREEN + "Downloading \"settings.xml\"")
			getFile("https://www.lawlsec.org/skynet/dev/settings.xml", os.path.join(Path.home(), ".m2", "settings.xml"))
		else:
			print(Fore.RED + "Skipping \"settings.xml\"" + Style.RESET_ALL)
			print(Fore.RED + "You may not be able to use SkyNetwork SDK" + Style.RESET_ALL)
			print(Fore.RED + "Manually move or merge \"settings.xml\" to your \".m2\" directory for SkyNetwork SDK support" + Style.RESET_ALL)
	else:
		print(Fore.RED + "Unable to locate Maven \".m2\" directory!" + Style.RESET_ALL)
		print(Fore.RED + "You may not be able to use SkyNetwork SDK" + Style.RESET_ALL)
		print(Fore.RED + "Manually move or merge \"settings.xml\" to your \".m2\" directory for SkyNetwork SDK support" + Style.RESET_ALL)

if __name__ == "__main__":
   main(sys.argv[1:])